using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace Client
{
    public class Program
    {
        public static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();

        private static async Task<DiscoveryResponse> Discover()
        {
            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                throw new Exception(disco.Error);
            }
            return disco;
        }

        private static async Task<string> GetClientCredentialsToken()
        {
            var disco = await Discover();

            // request token
            var tokenClient = new TokenClient(disco.TokenEndpoint, "client", "secret");
            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api1");

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                throw new Exception(tokenResponse.Error);
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");

            return tokenResponse.AccessToken;
        }

        private static async Task<string> GetResourseOwnerToken()
        {
            var disco = await Discover();

            var tokenClient = new TokenClient(disco.TokenEndpoint, "ro.client", "secret");
            var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync("bob", "password", "api1");

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                throw new Exception(tokenResponse.Error);
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");
            return tokenResponse.AccessToken;
        }

        private static async Task MainAsync()
        {
            Console.WriteLine("press a key to continue");
            var Wait = Console.ReadLine();

            //var accessToken = await GetClientCredentialsToken();
            var accessToken = await GetResourseOwnerToken();

            // call api
            var client = new HttpClient();
            client.SetBearerToken(accessToken);

            var response = await client.GetAsync("http://localhost:5001/identity");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));

                Console.WriteLine("got identity from api, getting data");

                var dataResponse = await client.GetAsync("http://localhost:5001/api/values");
                if (!dataResponse.IsSuccessStatusCode)
                {
                    Console.WriteLine(dataResponse.StatusCode);
                }
                else
                {
                    var dataContent = await dataResponse.Content.ReadAsStringAsync();
                    Console.WriteLine(JArray.Parse(dataContent));

                    Console.WriteLine("got data");


                    Console.WriteLine("press a key to exit");
                    Console.ReadLine();
                }

                Console.WriteLine("press a key to continue");
                var Wait2 = Console.ReadLine();
            }

            
        }
    }
}
